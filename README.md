## General todos

* Figure out how to generalize the model to any view (not just style-elements)
* Improve the design of the UI
  * Play around with scales offered by styled elephants
  * Use a good grey accessible colorscheme
* Provide keyboard shortcuts for all kind of actions

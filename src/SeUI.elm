module SeUI
    exposing
        ( Model
        , Msg(..)
        , UIElement(..)
        , addNested
        , addSingle
        , empty
        , init
        , reverse
        , setElementTree
        , setInternMsgsToElement
        , single
        , smain
        , uiElementMap
        , update
        , view
        )

import Color
import Element as E
import Element.Area as EArea
import Element.Background as EBackground
import Element.Border as EBorder
import Element.Events as EEvents
import Element.Font as EFont
import Element.Input as EInput
import Html exposing (Html)
import List


{-| Use this as custom main function. You need to pass in a SeUI.Modell for that.
-}
smain : ( Model msg, Cmd (Msg msg) ) -> Program Never (Model msg) (Msg msg)
smain init =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        }



---- Model ----


{-| Main model used for the Style elements UI.
-}
type alias Model msg =
    { activeElement : Maybe (E.Element (Msg msg))
    , elementTree : List (UIElement (Msg msg))
    , internMsgs : List msg
    , internMsgToElement : Maybe (msg -> E.Element (NoMsg msg))
    , searchTerm : Maybe String
    }


{-| Initializes an empty model. Combine this with setElementTree and setInternalMsgsToElement
-}
init : Model msg
init =
    { activeElement = Nothing
    , elementTree = []
    , internMsgs = []
    , internMsgToElement = Nothing
    , searchTerm = Nothing
    }


setElementTree : List (UIElement msg) -> Model msg -> Model msg
setElementTree elements model =
    { model | elementTree = List.map (uiElementMap InternMsg) elements }


setInternMsgsToElement : (msg -> E.Element (NoMsg msg)) -> Model msg -> Model msg
setInternMsgsToElement internMsgToElement model =
    { model | internMsgToElement = Just internMsgToElement }


type NoMsg msg
    = NoMsg


noMsgToMsg : NoMsg msg -> Msg msg
noMsgToMsg _ =
    Noop


type alias Element msg =
    E.Element msg


elementMap : (msg -> msg1) -> Element msg -> Element msg1
elementMap fn element =
    E.map fn element


type UIElement msg
    = SingleUIElement String (Element msg)
    | NestedUIElements String Bool (List (UIElement msg))


empty : List (UIElement msg)
empty =
    []


single : String -> Element msg -> UIElement msg
single =
    SingleUIElement


addSingle : String -> Element msg -> List (UIElement msg) -> List (UIElement msg)
addSingle string element list =
    single string element :: list


nested : String -> List (UIElement msg) -> UIElement msg
nested string elements =
    NestedUIElements string True elements


addNested : String -> List (UIElement msg) -> List (UIElement msg) -> List (UIElement msg)
addNested string elements elements_ =
    nested string elements :: elements_


reverse : List (UIElement msg) -> List (UIElement msg)
reverse elements =
    List.map
        (\element ->
            case element of
                SingleUIElement string element_ ->
                    SingleUIElement string element_

                NestedUIElements string bool elements_ ->
                    NestedUIElements string bool (reverse elements_)
        )
        elements
        |> List.reverse


uiElementMap : (msg -> msg1) -> UIElement msg -> UIElement msg1
uiElementMap fn element =
    case element of
        SingleUIElement string element ->
            SingleUIElement string (elementMap fn element)

        NestedUIElements string open elements ->
            NestedUIElements string open (List.map (uiElementMap fn) elements)


setOpenState : UIElement msg -> Bool -> List (UIElement msg) -> List (UIElement msg)
setOpenState element open elements =
    List.map
        (\tree ->
            case tree of
                SingleUIElement _ _ ->
                    tree

                NestedUIElements string open_ subTree ->
                    if element == tree then
                        NestedUIElements string open subTree
                    else
                        NestedUIElements string open_ (setOpenState element open subTree)
        )
        elements


isJust : Maybe a -> Bool
isJust maybe =
    case maybe of
        Just _ ->
            True

        Nothing ->
            False


filterJust : List (Maybe a) -> List a
filterJust list =
    List.foldl
        (\x xs ->
            case x of
                Just x_ ->
                    x_ :: xs

                Nothing ->
                    xs
        )
        []
        list
        |> List.reverse


filterElement : (UIElement msg -> Bool) -> UIElement msg -> Maybe (UIElement msg)
filterElement predicate tree =
    case tree of
        SingleUIElement string_ element ->
            if predicate tree then
                Just tree
            else
                Nothing

        NestedUIElements string_ open elements ->
            -- @todo This logic is not right yet
            let
                filtered =
                    List.map (filterElement predicate) elements
                        |> filterJust
            in
            if List.length filtered == 0 then
                Nothing
            else
                Just (NestedUIElements string_ open filtered)


containsStringPredicate : String -> UIElement msg -> Bool
containsStringPredicate string tree =
    case tree of
        SingleUIElement string_ element ->
            String.contains string string_

        NestedUIElements string_ open elements ->
            String.contains string string_ || List.any (containsStringPredicate string) elements



---- UPDATE ----


type Msg msg
    = Noop
    | InternMsg msg
    | SelectElement (E.Element (Msg msg))
    | OpenNestedElement (UIElement (Msg msg))
    | CloseNestedElement (UIElement (Msg msg))
    | SearchElement String


update : Msg msg -> Model msg -> ( Model msg, Cmd (Msg msg) )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )

        InternMsg msg ->
            ( { model | internMsgs = msg :: model.internMsgs }, Cmd.none )

        SelectElement element ->
            ( { model | activeElement = Just element }, Cmd.none )

        OpenNestedElement element ->
            ( { model
                | elementTree = setOpenState element True model.elementTree
              }
            , Cmd.none
            )

        CloseNestedElement element ->
            ( { model
                | elementTree = setOpenState element False model.elementTree
              }
            , Cmd.none
            )

        SearchElement string ->
            ( { model
                | searchTerm = Just string
              }
            , Cmd.none
            )



---- VIEW ----


colorBackground : Color.Color
colorBackground =
    Color.lightGray


colorSelected : Color.Color
colorSelected =
    Color.darkGray


colorBoxes : Color.Color
colorBoxes =
    Color.white


colorBoxesRadius : Color.Color
colorBoxesRadius =
    Color.gray


roundedBox =
    [ E.width E.fill
    , EBorder.solid
    , EBorder.rounded 3
    , EBorder.width 1
    , EBorder.color colorBoxesRadius
    , EBackground.color colorBoxes
    ]


view : Model msg -> Html (Msg msg)
view model =
    E.layout
        [ E.height E.fill
        , E.alignTop
        , EBackground.color colorBackground
        ]
    <|
        E.row [ E.height E.fill, E.alignTop ]
            [ E.column
                [ E.width (E.fillPortion 1)
                , E.height E.fill
                , E.alignTop
                ]
                [ viewSearchBox model
                , Maybe.map
                    (\string ->
                        List.map (filterElement (containsStringPredicate string)) model.elementTree
                            |> filterJust
                    )
                    model.searchTerm
                    |> Maybe.withDefault model.elementTree
                    |> viewRootTree model.activeElement
                ]
            , E.column
                [ E.width (E.fillPortion 4)
                , E.padding 5
                , E.spacing 5
                , E.center
                , E.alignTop
                ]
                [ E.el
                    (E.height E.fill :: roundedBox)
                  <|
                    E.el [ E.padding 50 ] (viewActiveElement model)
                , E.el
                    (E.height (E.px 200) :: roundedBox)
                    (viewMessages
                        model
                    )
                ]
            ]


viewMessages : Model msg -> E.Element (Msg msg)
viewMessages model =
    case model.internMsgToElement of
        Just internMsgToElement ->
            E.column []
                [ E.el
                    [ E.alignLeft
                    , E.width E.fill
                    , E.padding 10
                    , EBorder.widthEach
                        { top = 0
                        , left = 0
                        , right = 0
                        , bottom = 1
                        }
                    , EBorder.color colorBoxesRadius
                    ]
                    (E.el
                        [ EArea.heading 2, E.alignLeft ]
                        (E.text "Messages")
                    )
                , E.column [ E.alignLeft, E.padding 10, E.spacing 10 ] <| List.map (internMsgToElement >> E.map noMsgToMsg >> E.el [ E.alignLeft ]) model.internMsgs
                ]

        Nothing ->
            E.empty


viewSearchBox : Model msg -> E.Element (Msg msg)
viewSearchBox model =
    EInput.search []
        { onChange = Just SearchElement
        , text = model.searchTerm |> Maybe.withDefault ""
        , placeholder = Just (EInput.placeholder [] (E.text "Search"))
        , label = EInput.labelAbove [ E.hidden True ] (E.text "Search")
        , notice = Nothing
        }


viewRootTree : Maybe (E.Element (Msg msg)) -> List (UIElement (Msg msg)) -> E.Element (Msg msg)
viewRootTree activeElement elements =
    E.column
        [ E.alignLeft
        , E.paddingXY 0 10
        , EArea.navigation
        ]
        (List.map (viewTree activeElement) elements)


viewTree : Maybe (E.Element (Msg msg)) -> UIElement (Msg msg) -> E.Element (Msg msg)
viewTree activeElement uiElements =
    case uiElements of
        SingleUIElement string element ->
            let
                isActive =
                    Maybe.map ((==) element) activeElement |> Maybe.withDefault False
            in
            E.el
                [ E.alignLeft
                , E.padding 10
                , E.width E.fill
                , if isActive then
                    EBackground.color colorSelected
                  else
                    E.hidden False
                , EEvents.onClick (SelectElement element)
                , E.pointer
                ]
                (E.el
                    [ E.alignLeft
                    , if isActive then
                        EFont.bold
                      else
                        E.hidden False
                    ]
                    (E.text string)
                )

        NestedUIElements string open elements ->
            let
                openIndicator =
                    if open then
                        E.el
                            [ E.alignLeft
                            , E.width E.fill
                            , EEvents.onClick (CloseNestedElement uiElements)
                            , E.pointer
                            ]
                            (E.text (string ++ " ↴"))
                    else
                        E.el
                            [ E.alignLeft
                            , E.width E.fill
                            , EEvents.onClick (OpenNestedElement uiElements)
                            , E.pointer
                            ]
                            (E.text (string ++ " →"))
            in
            E.column
                [ E.alignLeft
                , E.padding 10
                , E.width E.fill
                ]
                [ openIndicator
                , if open then
                    E.column [ E.width E.fill ]
                        (List.map
                            (E.el
                                [ E.width E.fill
                                , E.paddingXY 10 0
                                ]
                            )
                            (List.map (viewTree activeElement) elements)
                        )
                  else
                    E.empty
                ]


viewActiveElement : Model msg -> E.Element (Msg msg)
viewActiveElement model =
    case model.activeElement of
        Nothing ->
            E.text "Please select an element from the left side"

        Just element ->
            element

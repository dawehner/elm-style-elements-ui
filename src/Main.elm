module Main exposing (..)

import Element as E
import Element.Events as EEvents
import SeUI exposing (UIElement(..), addNested, addSingle, empty, single)


type MyMsg
    = MyClick String


myMsgToElement : MyMsg -> E.Element msg
myMsgToElement a =
    case a of
        MyClick string ->
            E.text string


viewH1 : String -> E.Element MyMsg
viewH1 string =
    E.text string


viewWithClick : String -> E.Element MyMsg
viewWithClick string =
    E.el [ EEvents.onClick (MyClick string) ] (E.text string)


exampleTree : List (UIElement MyMsg)
exampleTree =
    empty
        |> addSingle
            "n0"
            (viewH1 "n0")
        |> addNested
            "n1"
            ([ single "n1.1" (viewH1 "n1.1")
             , single "n1.2" (viewH1 "n1.2")
             ]
                |> addNested "n1.3"
                    [ single "n1.3.1" (viewH1 "n1.3.1")
                    , single "n1.3.2" (viewH1 "n1.3.2")
                    ]
            )
        |> addSingle "n2" (viewH1 "n2")
        |> addSingle "n3" (viewWithClick "n3")
        |> SeUI.reverse


init : ( SeUI.Model MyMsg, Cmd (SeUI.Msg MyMsg) )
init =
    ( SeUI.init
        |> SeUI.setElementTree exampleTree
        |> SeUI.setInternMsgsToElement myMsgToElement
    , Cmd.none
    )


main =
    SeUI.smain init
